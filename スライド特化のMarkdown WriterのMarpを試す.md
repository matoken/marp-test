<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->
![bg](openlogo-nd.svg)

# Marpのテストスライド

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 

---

# hello! Marp
- マルチプラットホーム&スライドに特化したMarkdown Writer!
	- [Marp - Markdown Presentation Writer](https://yhatt.github.io/marp/ "Marp - Markdown Presentation Writer")
- ライセンスは*MIT License.*
	- [yhatt/marp: Markdown presentation writer, powered by Electron.](https://github.com/yhatt/marp/ "yhatt/marp: Markdown presentation writer, powered by Electron.")
- PDFに書き出すことが出来る

---

# Linux amd64への導入例

- アーカイブを入手して適当な場所に展開する

```shell
$ wget https://github.com/yhatt/marp/releases/download/v0.0.10/0.0.10-Marp-linux-x64.tar.gz
$ sha512sum 0.0.10-Marp-linux-x64.tar.gz
9e2d37812f71958478e89ec13c85eddd935bf9de957ae7d374c3b078c5ab7ec1a2f9bf98e66e5c85099bff957244d73dc8ce30f64bff35d4ffdc999872ae7ecb  0.0.10-Marp-linux-x64.tar.gz
$ sha256sum 0.0.10-Marp-linux-x64.tar.gz
1d5e7b2c134c6639ef49d943ce3a1fb95bb0d618bbdb2f826608e1ed57a39874  0.0.10-Marp-linux-x64.tar.gz
$ tar tvzf 0.0.10-Marp-linux-x64.tar.gz
$ mkdir ~/usr/local/Marp
$ tar xf 0.0.10-Marp-linux-x64.tar.gz -C ~/usr/local/Marp
```

---

# 実行

- 展開した中の`Marp`を実行する

```
$ ~/usr/local/Marp/Marp
```

---

# Syntax

- 詳細は以下のページを参照(Marp形式なのでMarpで開いてSlide Listで読むと読みやすい)
 [https://raw.githubusercontent.com/yhatt/marp/master/example.md](https://raw.githubusercontent.com/yhatt/marp/master/example.md "https://raw.githubusercontent.com/yhatt/marp/master/example.md")

---

# スライドで便利そうなsyntax

---

## ページの表示非表示

```
<!-- page_number: true -->
```

- 既定値は`false`
- ページごとに設定可能

---

## アスペクト比&用紙サイズ

```
<!-- $size: 16:9 -->
```
- 以下のようなものが設定できる
	> Presets: `4:3`, `16:9`, `A0`-`A8`, `B0`-`B8` and suffix of `-portrait`.
- 複数書いた場合最後に書いたものだけが有効になった

---
<!-- footer: フッターを変更 -->

## フッターの指定

- 書いたページ以降で有効になる
- 複数回指定可能(途中からフッターを変えることが出来る)

```
<!-- footer: フッター -->
```

---
![bg](openlogo-nd.svg)

## 背景の指定

- 設定したいページ毎に指定が必要

```
![bg](openlogo-nd.svg)
```

